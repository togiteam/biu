import {Component} from 'angular2/core';
import {AnimalService, Animal} from "./services/animal.service";
import {AnimalDetailComponent} from "./components/animal.detail.component";


@Component({
    providers: [AnimalService],
    selector: 'my-app',
    directives: [AnimalDetailComponent],
    template: `
        <div class="container">
            <div>
                <h2>Stan zwierząt w ZOO:</h2>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <ul>
                        <li *ngFor="#animal of animals" (click)="onSelect(animal)">{{animal.name}} - {{animal.likes}}</li>
                    </ul>
                </div>
                <div class="col-lg-8" *ngIf="showDetails">
                    <animal-detail [animalDetail]="selectedAnimal"></animal-detail>
                </div>
            </div>
        </div>
    `
})
export class AppComponent {
    public animals;
    public selectedAnimal:Animal;
    public showDetails:boolean = false;

    constructor(as: AnimalService){
        this.animals = as.getAnimals();
    }

    onSelect(animal:Animal){
        this.selectedAnimal = animal;
        this.showDetails = true;
        console.log("wybrany zwierz: " + JSON.stringify(animal));
    }
}