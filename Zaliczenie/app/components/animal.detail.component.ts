/**
 * Created by togi on 31.01.16.
 */
import {Component, Input} from 'angular2/core';
import {Animal} from '../services/animal.service';

@Component({
    selector: 'animal-detail',
    template: `
        <div>
            <div>
                <h2>Szczegóły:</h2>
            </div>
            <div>
                <div>
                    <label>Imię:</label>
                    <span>{{ animalDetail.name }}</span>
                </div>
                <div>
                    <label>Wiek:</label>
                    <span>{{ animalDetail.age }}</span>
                </div>
                <div>
                    <label>Rasa:</label>
                    <span>{{ animalDetail.race }}</span>
                </div>
            </div>
        </div>
    `
})
export class AnimalDetailComponent {

    @Input()
    public animalDetail: Animal;

    constructor(){
    }
}