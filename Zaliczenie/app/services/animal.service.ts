/**
 * Created by togi on 31.01.16.
 */
import {Injectable} from 'angular2/core'

export interface Animal {
    id: number;
    name: string;
    race: string;
    age: number;
    likes: number;
}

var ZooAnimals: Animal[] = [
    {
        "id": 1,
        "name": "Bolek",
        "race": "Lama",
        "age": 1,
        "likes": 0
    },
    {
        "id": 2,
        "name": "Lolek",
        "race": "Zebra",
        "age": 1,
        "likes": 0
    },
    {
        "id": 1,
        "name": "Tola",
        "race": "Lew",
        "age": 1,
        "likes": 0
    },
];

@Injectable()
export class AnimalService {
    getAnimals(){
        return ZooAnimals;
    }

    //getAnimal(animalId:number){
    //    return ZooAnimals.map(function(x:Animal){
    //        return x;
    //    }).filter(function(x){
    //        if(x.id == animalId){
    //            return x;
    //        }
    //    });
    //}
    //
    //setAnimalAge(animalId:number, animalAge:number){
    //    var animal:Animal = this.getAnimal(animalId);
    //
    //    animal.age = animalAge;
    //
    //    return animal;
    //}
    //
    //setAnimalLikes(animalId:number, upLike:boolean){
    //    var animal:Animal = this.getAnimal(animalId);
    //
    //    if(upLike){
    //        animal.likes += 1;
    //    } else {
    //        animal.likes -= 1;
    //    }
    //
    //    return animal;
    //}
}