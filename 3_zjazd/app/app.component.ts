import {Component} from 'angular2/core';
import {HTTP_PROVIDERS, Http} from 'angular2/http'
import {PersonService, Person} from './services/person.service'
import {Observable} from 'rxjs/Observable';
import {Response} from "angular2/http";


@Component({
    providers: [PersonService, HTTP_PROVIDERS],
    selector: 'my-app',
    template: `
        <h2>{{message}}</h2>
        <div *ngIf="selectedPerson">
            <input [(ngModel)]="selectedPerson.name" />
        </div>
        <div>
            <ul>
                <li *ngFor="#person of persons" (click)="onSelect(person)">{{person.name}}</li>
            </ul>
        </div>
        <button type="button" (click)="makeHttpRequest()">Make Request</button>

        <div *ngIf="loading">Loading... please wait</div>

        <pre>{{ data | json }}</pre>

        <div>
            <input [(ngModel)]="bodyContent" (keyup)="searchComments(bodyContent)"/>
        </div>

        <pre> {{ resComments | json }}</pre>
    `
})
export class AppComponent {
    public loading: Boolean;

    public bodyContent: string;
    public comments: Object;

    public resComments: Object[] = [];

    public data: Object;

    public persons;
    public message: string = "Hello world! (with services)";

    public selectedPerson: Person;

    constructor(ps: PersonService, private _http: Http){
        this.persons = ps.getPersons();
    }

    onSelect(person: Person){
        this.selectedPerson = person;
        console.log('Selected person: ' + person.name);
    }

    makeHttpRequest(){
        this.loading = true;
        this._http.request('http://jsonplaceholder.typicode.com/posts/1').subscribe((res:Response) => {
            this.data = res.json();
            this.loading = false;
        });
    }

    // Dokończyć filtrowanie wiadomości
    searchComments(bodyContent){
        console.log('event');
        console.log('search: ' + bodyContent);
        this._http.request('http://jsonplaceholder.typicode.com/comments').subscribe((res:Response) => {
            this.resComments = res.json()
                .map(function(x){
                    if(x.body.search(bodyContent) > 0){
                        console.log('found comment');
                        return x;
                    }
                })
                .filter(function(x){
                    if(x != null){
                        return x
                    }
                });
            console.log('length of comments array: ' + this.resComments.length)
        });
    }
}