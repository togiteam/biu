import {Component} from 'angular2/core';
import {MyButtonComponent} from './components/button.component';
import {btnMessage} from "./components/button.component";


@Component({
    selector: 'my-app',
    directives: [MyButtonComponent],
    template: `
        <div>
            <my-button (btnAction)="myAction($event)" [btnLabel]="myLabel"></my-button>
        </div>
    `
})
export class AppComponent {
    public myLabel = "Say: Hello";

    constructor(){

    }

    myAction(data: btnMessage){
        console.log("Otrzymalem: " + data.message + " - " + data.date);
    }
}