/**
 * Created by togi on 20.12.15.
 */

var q = $('#q');
var loader = $('#loader');

var source = Rx.Observable.fromEvent(q, 'keyup');

var observer = Rx.Observer.create(function(x){
    x[1].forEach(function(el, index){
        $('#result_list').append('<li><a href="' + x[3][index] + '" >' + el + '</a></li>');
    });
});

source.throttle(500)
    .map(function(x){
        return q.val();
    })
    .do(function(x){
        loader.show();
    })
    .flatMapLatest(function(query){
        return search_wikipedia(query);
    })
    .do(
        function(x){
            loader.hide();
        }
    )
    .subscribe(observer);

function search_wikipedia(query){
    return $.ajax({
        url: "http://pl.wikipedia.org/w/api.php",
        dataType: "jsonp",
        data: {
            action: 'opensearch',
            format: 'json',
            search: query
        }
    }).promise()
};

var bin = $('#bin');

var observer_bin = Rx.Observer.create(function(x){
    $('#bin_result').text(Number(x).toString(2));
    $('#hex_result').text(Number(x).toString(16));
});

var bin_source = Rx.Observable.fromEvent(bin, 'keyup');

bin_source
    .map(function(x){
        return bin.val();
    })
    .subscribe(observer_bin);