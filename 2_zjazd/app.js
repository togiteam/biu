/**
 * Created by togi on 20.12.15.
 */
//Cwiczenia
var source = Rx.Observable.fromEvent($(document), 'mousemove');

var observer = Rx.Observer.create(function(x){
    $('#result').text(x.clientX + ', ' + x.clientY);
});

source.map(function(e) {
    e.response = ''
    if (!(e.clientX < 200 && e.clientY < 200)){
        e.clientX = 'Poza zakresem';
        e.clientY = 'Poza zakresem';
    }
    return e;
}).subscribe(observer);