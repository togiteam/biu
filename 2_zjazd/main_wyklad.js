/**
 * Created by togi on 20.12.15.
 */

var RX = require('rx');

//var observable = RX.Observable.create(function(observer){
//    observer.onNext("Hello ");
//    observer.onNext("World ");
//    observer.onNext("! ");
//    observer.onCompleted();
//}).map(function(x){
//    return "M: " + x
//}).filter(function(x){
//    return x.length > 5;
//});

var observable = RX.Observable.range(0, 3).map(function(x){
    return 10 + x
}).filter(function(x){
    return x > 10;
});

var observer = RX.Observer.create(
    function(response){
        console.log("Next: ", response);
    },
    function(error){
        console.log("Error: " + err)
    },
    function(complete){
        console.log("Completed")
    });

var subscription = observable.subscribe(
    function(response){
        console.log("Next: ", response);
    },
    function(error){
        console.log("Error: " + err)
    },
    function(complete){
        console.log("Completed")
    });
