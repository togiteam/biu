import {Component} from 'angular2/core';
import {FORM_DIRECTIVES, ControlGroup, Control, FormBuilder, AbstractControl, Validators} from "angular2/common";


@Component({
    selector: 'my-app',
    directives: [FORM_DIRECTIVES],
    template: `
        <div>
            <form [ngFormModel]="myForm" (ngSubmit)="mySubmitAction(myForm.value)">
                <div>
                    <input type="text" placeholder="Podaj wartość" [ngFormControl]="myValue">
                    <div *ngIf="myValue.hasError('required')">Wartość wymagana</div>
                    <input type="text" placeholder="Podaj wartość" [ngFormControl]="myValue2">
                    <div *ngIf="myValue2.hasError('required')">Wartość wymagana</div>
                    <div *ngIf="myValue2.hasError('BolekValue')">Nie może być Bol</div>
                </div>
                <button type="submit">Submit!</button>
            </form>
        </div>
    `
})
export class AppComponent {
    myForm: ControlGroup;

    myValue: AbstractControl;
    myValue2: AbstractControl;

    constructor(fb: FormBuilder){
        this.myForm = fb.group({
            'myValue': ['Bolek', Validators.required],  //Pojedynczy walidator
            'myValue2': ['Lolek', Validators.compose([
                Validators.required,
                this.myValue2Validator
            ])] //Wiele walidatorow
        });
        this.myValue = this.myForm.controls['myValue']
        this.myValue2 = this.myForm.controls['myValue2']

        this.myValue.valueChanges.subscribe(
            (value: string) => {
                console.log("from observable: %s", value);
            }
        )
    }

    mySubmitAction(data: any){
        console.log('Wprowadzone dane w formularzu: ' + data.myValue + ", " + data.myValue2);
    }

    myValue2Validator(control: Control){
        if(control.value.match(/^Bol/)){
            return {'BolekValue': true}
        }
    }

}