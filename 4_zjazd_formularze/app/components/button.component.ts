import {Component} from 'angular2/core';
import {EventEmitter} from 'angular2/core';


@Component({
    selector: 'my-button',
    inputs: ['btnLabel'],
    outputs: ['btnAction'],
    template: `
        <div>
            <button type="button" (click)="action()">{{ btnLabel }}</button>
        </div>
    `
})
export class MyButtonComponent {
    public btnLabel: string;
    public btnAction: EventEmitter<btnMessage> = new EventEmitter();

    constructor(){

    }

    action(){
        var data: btnMessage = {
            message: "Hello!",
            date: new Date()
        };

        console.log('Emituje zdarzenie');

        this.btnAction.emit(data);
    }
}

export interface btnMessage{
    message: string;
    date: Date;
}