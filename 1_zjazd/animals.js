/**
 * Created by togi on 06.12.15.
 */
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Skin;
(function (Skin) {
    Skin[Skin["Fur"] = 1] = "Fur";
    Skin[Skin["Husk"] = 2] = "Husk";
    Skin[Skin["Skin"] = 3] = "Skin";
})(Skin || (Skin = {}));
;
var Animal = (function () {
    function Animal(species_in, skin_type_in, how_many_legs_in, has_wings_in, how_many_eyes_in, how_many_ears_in) {
        this.species = species_in || "Animal";
        this.skin_type = skin_type_in ? Skin[skin_type_in] : Skin[3];
        this.how_many_legs = how_many_legs_in || 0;
        this.has_wings = has_wings_in || false;
        this.how_many_eyes = how_many_eyes_in || 0;
        this.how_many_ears = how_many_ears_in || 0;
    }
    Animal.prototype.canItRun = function () {
        return Boolean(this.how_many_legs);
    };
    Animal.prototype.canItFly = function () {
        return this.has_wings;
    };
    Animal.prototype.move = function (distance) {
        var moved = distance || 1;
        return "Moved " + moved + " meters";
    };
    Animal.prototype.fight = function (result) {
        if (result && this.how_many_ears >= 0.5) {
            this.how_many_ears -= 0.5;
            return "Fight won but lost 0.5 ear";
        }
        else {
            if (this.how_many_legs >= 0.5) {
                this.how_many_legs -= 0.5;
                return "Fight lost and lost 0.5 leg";
            }
            return "Fight lost";
        }
    };
    return Animal;
})();
var Cat = (function (_super) {
    __extends(Cat, _super);
    function Cat(name_in, sound_in, hungry_in) {
        _super.call(this, "Cat", 1, 4, false, 2, 2);
        this.name = name_in;
        this.sound = sound_in;
        this.hungry = hungry_in;
    }
    return Cat;
})(Animal);
function checkMovementType(obj) {
    var run = obj.canItRun();
    var fly = obj.canItFly();
    if (run == true && fly == true) {
        return obj.name + " can run and fly";
    }
    else if (run == true && fly == false) {
        return obj.name + " can run but not fly";
    }
    else if (fly == true && run == false) {
        return obj.name + " can fly but not run";
    }
    else {
        return obj.name + " can not run or fly";
    }
}
function checkIsAnimalHungry(obj) {
    var response = obj.hungry ? "is" : "is not";
    return obj.name + " " + response + " hungry";
}
var aCat = new Cat("Puszek", "Mrrr", false);
console.log(checkMovementType(aCat));
console.log(checkIsAnimalHungry(aCat));
console.log(aCat.move(5));
