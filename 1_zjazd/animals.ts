/**
 * Created by togi on 06.12.15.
 */

enum Skin {Fur = 1, Husk, Skin};

class Animal{
    species: string;
    skin_type: Skin;
    how_many_legs: number;
    has_wings: boolean;
    how_many_eyes: number;
    how_many_ears: number;

    constructor(species_in?: string, skin_type_in?: number,
                how_many_legs_in?: number, has_wings_in?: boolean,
                how_many_eyes_in?: number, how_many_ears_in?: number){
        this.species = species_in || "Animal";
        this.skin_type = skin_type_in ? Skin[skin_type_in] : Skin[3];
        this.how_many_legs = how_many_legs_in || 0;
        this.has_wings = has_wings_in || false;
        this.how_many_eyes = how_many_eyes_in || 0;
        this.how_many_ears = how_many_ears_in || 0;
    }

    canItRun(){
        return Boolean(this.how_many_legs);
    }

    canItFly(){
        return this.has_wings;
    }

    move(distance?: number){
        var moved = distance || 1;
        return "Moved " + moved + " meters";
    }

    fight(result: boolean){
        if (result && this.how_many_ears >= 0.5){
            this.how_many_ears -= 0.5;
            return "Fight won but lost 0.5 ear";
        } else {
            if (this.how_many_legs >= 0.5) {
                this.how_many_legs -= 0.5;
                return "Fight lost and lost 0.5 leg"
            }
            return "Fight lost";
        }
    }

}

class Cat extends Animal implements CheckMovementInterface, IsHungry{
    sound: string;
    hungry: boolean;
    name: string;

    constructor(name_in: string, sound_in: string, hungry_in: boolean){
        super("Cat", 1, 4, false, 2, 2);
        this.name = name_in;
        this.sound = sound_in;
        this.hungry = hungry_in;
    }
}

interface CheckMovementInterface{
    name: string;
    canItRun(): boolean;
    canItFly(): boolean;
}

function checkMovementType(obj: CheckMovementInterface){
    var run = obj.canItRun();
    var fly = obj.canItFly();

    if (run == true && fly == true){
        return obj.name + " can run and fly";
    } else if (run == true && fly == false){
        return obj.name + " can run but not fly";
    } else if (fly == true && run == false){
        return obj.name + " can fly but not run";
    } else {
        return obj.name + " can not run or fly";
    }
}

interface IsHungry{
    name: string;
    hungry: boolean;
}

function checkIsAnimalHungry(obj: IsHungry){
    var response = obj.hungry ? "is" : "is not";
    return obj.name + " " + response + " hungry";
}

var aCat = new Cat("Puszek", "Mrrr", false);

console.log(checkMovementType(aCat));
console.log(checkIsAnimalHungry(aCat));

console.log(aCat.move(5));