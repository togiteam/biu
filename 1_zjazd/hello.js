/**
 * Created by togi on 06.12.15.
 */
var Person = (function () {
    function Person(name) {
        this.first_name = name;
    }
    Person.prototype.sayHello = function (message) {
        var text = message || "Hello";
        return text + " " + this.first_name;
    };
    return Person;
})();
var aPerson = new Person("Martin");
console.log(aPerson.sayHello());
var aDog = { name: "Burek" };
//printName(aDog);
printName(aPerson);
function printName(obj) {
    console.log(obj.sayHello("Witaj"));
}
