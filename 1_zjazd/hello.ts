/**
 * Created by togi on 06.12.15.
 */

class Person{

    first_name: string;

    constructor(name){
        this.first_name = name;
    }

    sayHello(message?: string) {
        var text = message || "Hello";
        return text + " " + this.first_name;
    }
}

var aPerson = new Person("Martin");

console.log(aPerson.sayHello());

var aDog = {name: "Burek"};

//printName(aDog);

printName(aPerson);

interface HaveFirstNameAndCanSayHello{
    first_name: string;
    sayHello(message?: string): string;
}

function printName(obj: HaveFirstNameAndCanSayHello){
    console.log(obj.sayHello("Witaj"));
}
